﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomActions.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixExtensions.Orchard.CustomActions
{
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.Deployment.WindowsInstaller;
    using Proligence.Tools.WixExtensions.Orchard.CustomActions.Actions;
    using Proligence.Tools.WixExtensions.Orchard.CustomActions.Infrastructure;

    /// <summary>
    /// Defines the custom actions exposed by the assembly.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Justification = "By design.")]
    public static class CustomActions
    {
        /// <summary>
        /// The custom action runner.
        /// </summary>
        private static readonly CustomActionRunner Runner = new CustomActionRunner();

        /// <summary>
        /// Assigns file system permissions required by Orchard applications.
        /// ORCHARDROOT - path to root directory of the Orchard application.
        /// ORCHARDUSER - the name of the user (identity) of the Orchard application's AppPool.
        /// </summary>
        /// <param name="session">The custom action session.</param>
        /// <returns>Custom action result.</returns>
        [CustomAction]
        public static ActionResult SetOrchardFileSystemPermissions(Session session)
        {
            return Runner.Execute<SetOrchardFileSystemPermissionsAction>(session);
        }
    }
}