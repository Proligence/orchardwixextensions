﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICustomAction.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixExtensions.Orchard.CustomActions.Infrastructure
{
    using Microsoft.Deployment.WindowsInstaller;

    /// <summary>
    /// Defines the interface for classes which implement Wix custom actions.
    /// </summary>
    public interface ICustomAction
    {
        /// <summary>
        /// Executes the custom action.
        /// </summary>
        /// <param name="session">The custom action session.</param>
        /// <returns>Custom action result.</returns>
        ActionResult Execute(Session session);
    }
}