﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomActionRunner.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixExtensions.Orchard.CustomActions.Infrastructure
{
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.Deployment.WindowsInstaller;

    /// <summary>
    /// Executes Wix custom actions.
    /// </summary>
    public class CustomActionRunner
    {
        /// <summary>
        /// Executes the specified custom action.
        /// </summary>
        /// <typeparam name="TAction">The type which implements the custom action to execute.</typeparam>
        /// <param name="session">The custom action session.</param>
        /// <returns>Custom action result.</returns>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "By design.")]
        public ActionResult Execute<TAction>(Session session)
            where TAction : ICustomAction, new()
        {
            var action = new TAction();
            return action.Execute(session);
        }
    }
}