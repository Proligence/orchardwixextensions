﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetOrchardFileSystemPermissionsAction.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixExtensions.Orchard.CustomActions.Actions
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Security.AccessControl;
    using Microsoft.Deployment.WindowsInstaller;
    using Proligence.Tools.WixExtensions.Orchard.CustomActions.Infrastructure;

    /// <summary>
    /// Implements a custom action which assigns file system permissions required by Orchard applications.
    /// </summary>
    public class SetOrchardFileSystemPermissionsAction : ICustomAction
    {
        /// <summary>
        /// Standard read-only file system permissions.
        /// </summary>
        private const FileSystemRights ReadPermissions = 
            FileSystemRights.Read | 
            FileSystemRights.ReadAndExecute | 
            FileSystemRights.ListDirectory;

        /// <summary>
        /// Standard read-write file system permissions.
        /// </summary>
        private const FileSystemRights ReadWritePermissions = 
            FileSystemRights.Read | 
            FileSystemRights.Write | 
            FileSystemRights.ReadAndExecute | 
            FileSystemRights.ListDirectory;

        /// <summary>
        /// Assigns file system permissions required by Orchard applications.
        /// ORCHARDROOT - path to root directory of the Orchard application.
        /// ORCHARDUSER - the name of the user (identity) of the Orchard application's AppPool.
        /// </summary>
        /// <param name="session">The custom action session.</param>
        /// <returns>Custom action result.</returns>
        public ActionResult Execute(Session session)
        {
            session.Log("Begin SetOrchardFileSystemPermissions");

            string orchardRoot = session.CustomActionData["ORCHARDROOT"];
            string orchardUser = session.CustomActionData["ORCHARDUSER"];

            if (string.IsNullOrEmpty(orchardRoot))
            {
                session.Log("ORCHARDROOT is empty.");
                return ActionResult.Failure;
            }

            if (string.IsNullOrEmpty(orchardUser))
            {
                session.Log("ORCHARDUSER is empty.");
                return ActionResult.Failure;
            }

            if (!Directory.Exists(orchardRoot))
            {
                session.Log("ORCHARDROOT directory ({0}) does not exist.", orchardRoot);
                return ActionResult.Failure;
            }

            string appDataPath = Path.Combine(orchardRoot, "App_Data");
            if (!Directory.Exists(appDataPath))
            {
                session.Log("Creating missing directory: " + appDataPath);
                Directory.CreateDirectory(appDataPath);
            }

            string mediaPath = Path.Combine(orchardRoot, "Media");
            if (!Directory.Exists(mediaPath))
            {
                session.Log("Creating missing directory: " + mediaPath);
                Directory.CreateDirectory(mediaPath);
            }

            if (!AssignPermissions(session, orchardRoot, orchardUser, ReadPermissions))
            {
                return ActionResult.Failure;
            }

            if (!AssignPermissions(session, appDataPath, orchardUser, ReadWritePermissions))
            {
                return ActionResult.Failure;
            }

            if (!AssignPermissions(session, mediaPath, orchardUser, ReadWritePermissions))
            {
                return ActionResult.Failure;
            }

            return ActionResult.Success;
        }

        /// <summary>
        /// Assigns the specified permissions to a directory for a specified user.
        /// </summary>
        /// <param name="session">The custom action session.</param>
        /// <param name="path">The path to the directory for which permissions will be granted.</param>
        /// <param name="user">The name of the user for which permissions will be granted.</param>
        /// <param name="permissions">The permissions to grant.</param>
        /// <param name="accessType">The type of the access control entry.</param>
        /// <param name="propagation">Propagation flags</param>
        /// <param name="inheritance">Inheritance flags</param>
        /// <returns><c>true</c> if the permissions were succesfully granted; otherwise, <c>false</c>.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "By design.")]
        private static bool AssignPermissions(
            Session session,
            string path,
            string user,
            FileSystemRights permissions,
            AccessControlType accessType = AccessControlType.Allow,
            PropagationFlags propagation = PropagationFlags.None,
            InheritanceFlags inheritance = InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit)
        {
            session.Log(
                "Assigning {0} to directory {1} for user {2}",
                permissions.ToString(),
                path,
                user);

            try
            {
                DirectorySecurity ds = Directory.GetAccessControl(path);
                ds.AddAccessRule(new FileSystemAccessRule(user, permissions, inheritance, propagation, accessType));
                Directory.SetAccessControl(path, ds);
                return true;
            }
            catch (Exception ex)
            {
                string message = "Failed to assign permissions, path: {0}, user: {1}, message: {2}";
                session.Log(message, path, user, ex.Message);
                return false;
            }
        }
    }
}